function submituserexport() {
    var data = {
        'action': 'user_export'
    };

    jQuery('#user-export-button').hide();
    jQuery('#user-export-loading').show();

    jQuery.post(ajax_object.ajax_url, data, function(response) {
        jQuery('#user-export-loading').hide();
        jQuery('#user-export-button').show();
        if (response.length) {
            response = jQuery.parseJSON(response);
            file = 'data:application/octet-stream,' + encodeURIComponent(response.data);
            var link = document.createElement('a');
            if (typeof link.download === 'string') {
                link.href = file;
                link.download = response.name;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
            return;
        } else {
            alert('There was an error downloading users.');
        }
    });
}
