<?php
    use CCL\PC\Model\Consumer;
    use CCL\PC\Model\ExternalUser;

    class CLWP_UserExport {
        const TPL_HDR  = "first name,last name,email address,company name,industry,function,zip code,primary contact,created date,approved\n";
        const TPL_ROW  = "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n";
        const TPL_CSV  = 'touchpoint-users-%s.csv';
        const PATH_CSV = '/wp-content/uploads/temp/';


        public static function init() {
            if (is_admin()) {
                add_action('admin_menu',            array('CLWP_UserExport', 'admin_menu'));
                add_action('admin_init',            array('CLWP_UserExport', 'admin_init'));
                add_action('admin_enqueue_scripts', array('CLWP_UserExport', 'user_export_enqueue'));
                add_action('wp_ajax_user_export',   array('CLWP_UserExport', 'user_export_callback'));
            }
        }

        public static function admin_init() {
            register_setting(
                'userexport_options',
                'userexport_options',
                array('CLWP_UserExport', 'options_validate')
            );
        }

        // editor panel for this plugin
        public static function admin_menu() {
            add_options_page(
                'User Export',
                'User Export',
                'manage_options',
                'userexport_options',
                array('CLWP_UserExport', 'admin_menu_options')
            );
        }

        // editor planel contentand layout
        public static function admin_menu_options() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            printf(
                '<div class="wrap">' .
                '<h2>User Export</h2>' .
                '</div>' .
                '<div id="user-export-info">' .
                '<p>Click the button below to export a csv list of TouchPoint users.' .
                '</div>' .
                '<div id="user-export-form">' .
                '<form>' .
                '<input id="user-export-button" class="button button-primary" type="button" value="Export Users" onClick="submituserexport();" />' .
                '<img id="user-export-loading" src="%s" style="display: none;" />' .
                '</form>' .
                '</div>',
                plugins_url('/spinner.gif', __FILE__)
            );
        }

        // init the user export callback
        public static function user_export_enqueue($hook) {
        	wp_enqueue_script(
            	'ajax-handle-user',
            	plugins_url('/user_export.js', __FILE__),
            	array('jquery')
        	);

        	wp_localize_script(
        	    'ajax-handle-user',
        	    'ajax_object',
        	    array(
        	        'ajax_url' => admin_url('admin-ajax.php')
        	    )
    	    );
        }

        /**
         * user_export_callback
         *
         * Export the users, create the csv, and pass
         * the path to the file to the ajax caller.
         */
        public static function user_export_callback() {
            // generate the file name
            $csv = sprintf(self::TPL_CSV, date('Ymd'));
            $out = self::TPL_HDR;

            // get the users
            $res = get_users();

            foreach ($res as $user) {
                // get the user data or skip if it's incomplete
                $external_user = ExternalUser::findByExternalIdentifier($user->ID, ExternalUser::WP);
                if (empty($external_user)) continue;
                $consumer_id = $external_user->getConsumerId();
                if (empty($consumer_id)) continue;
                $consumer = Consumer::findById($consumer_id);

                // clean up industry and funtion before we implode them
                $industry = !empty($meta_data['industry'][0]) ? unserialize($meta_data['industry'][0]) : array();
                $function = !empty($meta_data['function'][0]) ? unserialize($meta_data['function'][0]) : array();

                $meta_data       = get_user_meta($user->ID, null, true);
                $email_address   = !empty($user->user_email)                ? $user->user_email                : null;
                $created_date    = !empty($user->user_registered)           ? $user->user_registered           : null;
                $approved        = !empty($meta_data['approved'])           ? $meta_data['approved'][0]        : null;
                $first_name      = !empty($meta_data['first_name'])         ? $meta_data['first_name'][0]      : null;
                $last_name       = !empty($meta_data['last_name'])          ? $meta_data['last_name'][0]       : null;
                $primary_contact = !empty($meta_data['primary_contact'])    ? $meta_data['primary_contact'][0] : null;
                $company_name    = !empty($meta_data['company'])            ? $meta_data['company'][0]         : null;
                $industry        = !empty($industry) && is_array($industry) ? implode(',', $industry)          : null;
                $function        = !empty($function) && is_array($function) ? implode(',', $function)          : null;

                // get the zip code from pref center first, or metadata if it's not there
                if (!empty($consumer) && is_object($consumer)) $zip_code = $consumer->getPostalCode();
                if (empty($zip_code)) $zip_code = get_user_meta($user->ID, 'zip_code', true);

                // stuff the data into the row template and send it to stdout
                $out .= sprintf(
                    self::TPL_ROW,
                    $first_name,
                    $last_name,
                    $email_address,
                    $company_name,
                    $industry,
                    $function,
                    $zip_code,
                    $primary_contact,
                    $created_date,
                    $approved
                );
            }

            // quit out and send the file name
            $return = json_encode(array('name' => $csv, 'data' => $out));
            die($return);
        }
    }

    CLWP_UserExport::init();
?>