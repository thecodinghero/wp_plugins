<?php
    class CLWP_RegisterLists {

        public static function init() {
            if (is_admin()) {
                add_action('admin_menu', array('CLWP_RegisterLists', 'admin_menu'));
                add_action('admin_init', array('CLWP_RegisterLists', 'admin_init'));
            }
        }

        public static function admin_init() {
            register_setting(
                'registerlists_options',
                'registerlists_options',
                array('CLWP_RegisterLists', 'options_validate')
            );

            add_settings_section(
                'registerlists_options_all',
                'Register Lists',
                array('CLWP_RegisterLists', 'options_desc_all'),
                'registerlists'
            );

            add_settings_field(
                'register_whiltelist',
                'Domain White List',
                array('CLWP_RegisterLists', 'options_field_whitelist'),
                'registerlists',
                'registerlists_options_all'
            );

            add_settings_field(
                'register_blacklist',
                'Domain Black List',
                array('CLWP_RegisterLists', 'options_field_blacklist'),
                'registerlists',
                'registerlists_options_all'
            );
        }

        /**
         * options_desc_all
         *
         * Top usage instructions.
         */
        public static function options_desc_all() {
            print("<p>Tasty beef.</p>");
        }

        // editor panel for this plugin
        public static function admin_menu() {
            add_options_page(
                'Register Lists',
                'Register Lists',
                'manage_options',
                'registerlists_options',
                array('CLWP_RegisterLists', 'admin_menu_options')
            );
        }

        // editor planel contentand layout
        public static function admin_menu_options() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            print(
                '<div class="wrap">' .
                '<h2>Register Lists</h2>' .
                '<form method="post" action="options.php">'
            );

            settings_fields('registerlists_options');
            do_settings_sections('registerlists');
            submit_button();
        }

        // domain white list
        public static function options_field_whitelist() {
            $options = get_option('registerlists_options');
            $options = implode(",\n", $options['whitelist']);

            printf(
                '<textarea name="registerlists_options[whitelist]" rows="10" cols="50">%s</textarea>',
                !empty($options) ? $options : null
            );
        }

        // domain black list
        public static function options_field_blacklist() {
            $options = get_option('registerlists_options');
            $options = implode(",\n", $options['blacklist']);

            printf(
                '<textarea name="registerlists_options[blacklist]" rows="10" cols="50">%s</textarea><br>',
                !empty($options) ? $options : null
            );
        }

        public static function options_validate($input) {
            $options   = get_option('registerlists_options');
            $whitelist = explode(',', $input['whitelist']);
            $blacklist = explode(',', $input['blacklist']);
            foreach ($whitelist as &$option) $option = trim($option);
            foreach ($blacklist as &$option) $option = trim($option);
            $options['whitelist'] = $whitelist;
            $options['blacklist'] = $blacklist;
            return($options);
        }


        /**
         * ____________________________________/\\\\\\_________________
         *  ___________________________________\////\\\_________________
         *   ___________________/\\\_______/\\\____\/\\\_________________
         *    __/\\\____/\\\__/\\\\\\\\\\\_\///_____\/\\\_____/\\\\\\\\\\_
         *     _\/\\\___\/\\\_\////\\\////___/\\\____\/\\\____\/\\\//////__
         *      _\/\\\___\/\\\____\/\\\______\/\\\____\/\\\____\/\\\\\\\\\\_
         *       _\/\\\___\/\\\____\/\\\_/\\__\/\\\____\/\\\____\////////\\\_
         *        _\//\\\\\\\\\_____\//\\\\\___\/\\\__/\\\\\\\\\__/\\\\\\\\\\_
         *         __\/////////_______\/////____\///__\/////////__\//////////__
         *
         *
         * Helper methods for checking domains and such.
         *
         */

        public static function whitelisted($email=null) {
            $email   = explode('@', $email);
            $domain  = !empty($email[1]) ? $email[1] : null;
            $options = get_option('registerlists_options');
            $options = !empty($options['whitelist']) ? $options['whitelist'] : array();
            if (in_array($domain, $options)) return(true);
            return(false);
        }

        public static function blacklisted($email=null) {
            $email   = explode('@', $email);
            $domain  = !empty($email[1]) ? $email[1] : null;
            $options = get_option('registerlists_options');
            $options = !empty($options['blacklist']) ? $options['blacklist'] : array();
            if (in_array($domain, $options)) return(true);
            return(false);
        }
    }

    CLWP_RegisterLists::init();
?>
