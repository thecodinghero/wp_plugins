<?php
    namespace CLWP\MultisiteUtils;

    /**
     * Methods to process admin menus
     */
    class Menu
    {
        /**
         * Adds a menu to admin settings
         */
        public static function init()
        {
            add_options_page(
                'Multisite Utils',
                'Multisite Utils',
                'manage_options',
                'multisiteutils_options',
                [__NAMESPACE__ . '\\Menu', 'optionsPage']
            );
        }

        /**
         * Defines the page of options for the plugin
         */
        public static function optionsPage()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            print(
                '<div class="wrap">' .
                    '<h2>Multisite Utils Settings</h2>' .
                    '<form method="post" action="options.php">'
            );
            settings_fields('multisiteutils_options');
            do_settings_sections('multisiteutils');
            submit_button();
            print(
                    '</form>' .
                '</div>'
            );
        }

        public static function sectionSocial()
        {
            print(
                '<p>Use the following code to display the drop-down style region selector shortcode for the current page:<br />' .
                '<pre>&lt;?= do_shortcode("[region_selector_dropdown]"); ?&gt;</pre></p>' .
                '<p>Enter the url for each social platform in the inputs below.</p>'
            );
        }

        public static function fieldsSocialinks()
        {
            $options = get_option('multisiteutils_options');
            $output  = sprintf(
                '<table>' .
                    '<tr>' .
                        '<td>Facebook</td>' .
                        '<td><input name="multisiteutils_options[social_links][facebook]" size="40" value="%s" /></td>' .
                    '</tr>' .
                    '<tr>' .
                        '<td>Instagram</td>' .
                        '<td><input name="multisiteutils_options[social_links][instagram]" size="40" value="%s" /></td>' .
                    '</tr>' .
                    '<tr>' .
                        '<td>Youtube</td>' .
                        '<td><input name="multisiteutils_options[social_links][youtube]" size="40" value="%s" /></td>' .
                    '</tr>' .
                '</table',
                !empty($options['social_links']['facebook']) ? $options['social_links']['facebook'] : null,
                !empty($options['social_links']['instagram']) ? $options['social_links']['instagram'] : null,
                !empty($options['social_links']['youtube']) ? $options['social_links']['youtube'] : null
            );

            print($output);
        }
    }
?>