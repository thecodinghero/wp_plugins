<?php
    namespace CLWP\MultisiteUtils;

    /**
     * Methods related to the admin area
     */
    class Admin
    {
        /**
         * Tasks executed when activating the plugin.
         */
        public static function activate() {}

        /**
         * Tasks executed when deactivating the plugin.
         */
        public static function deactivate() {}

        /**
         * Initialize when showing an admin page.
         */
        public static function init()
        {
            register_setting(
                'multisiteutils_options',
                'multisiteutils_options',
                [__NAMESPACE__ . '\\Admin', 'validateOptions']
            );

            add_settings_section(
                'multisiteutils_options',
                'Multisite Utils Settings',
                [__NAMESPACE__ . '\\Menu', 'sectionSocial'],
                'multisiteutils'
            );

            add_settings_field(
                'multisiteutils_options',
                'Social Links',
                [__NAMESPACE__ . '\\Menu', 'fieldsSocialinks'],
                'multisiteutils',
                'multisiteutils_options'
            );
        }

        /**
         * Check that the host looks like a host.
         */
        public static function validateOptions($input)
        {
            $output = [];
            foreach ($input['social_links'] as $site => $link) {
                $link = sanitize_text_field($link);
                $output[$site] = $link;
            }

            $options['social_links'] = $output;
            return($options);
        }
    }
?>