<?php
    namespace CLWP\MultisiteUtils;

    use \Timber\Timber as Timber;

    /**
     * Methods that setup and support the shortcodes created by this plugin
     */
    class Shortcode
    {
        /**
         * Registers all the shortcodes
         */
        public static function registerAll()
        {
            // enable the short codes
            add_shortcode('region_selector_dropdown', [static::class, 'region_select_dropdown']);
            add_shortcode('region_selector_modal', [static::class, 'region_select_modal']);
            add_shortcode('social_links_list', [static::class, 'social_links']);
        }

        /**
         * region_select_dropdown
         *
         * Display the region selector as a select box drop down.
         *
         * [region_selector_dropdown]
         */
        public static function region_select_dropdown($atts)
        {
            $blog  = get_blog_details();
            $sites = function_exists('get_sites') ? get_sites() : [];
            foreach ($sites as &$site) {
                $details = get_blog_details(['blog_id' => $site->blog_id]);
                $site->blogname = $details->blogname;
            }

            $out = '<select id="lang_choice" name="country-select" onchange="location=this.value;">';
            foreach ($sites as $site) {
                $out .= sprintf(
                    '<option value="//%s"%s>%s</option>',
                    $site->domain,
                    $site->blog_id == $blog->blog_id ? ' selected' : null,
                    $site->blogname
                );
            }
            $out .= '</select>';

            return($out);
        }

        /**
         * region_select_modal
         *
         * Display the region selector as a modal.
         *
         * [region_selector_modal]
         */
        public static function region_select_modal($atts)
        {
            // get the current site's infos
            $blog = get_blog_details();

            // init the context for the view
            $context = [
                'blogname'   => $blog->blogname,
                'asset_path' => sprintf('%s/img', rtrim(plugin_dir_url(dirname(__FILE__)), '/')),
                'theme_path' => rtrim(get_template_directory_uri(), '/')
            ];

            // stuff the context with all the other site's infos
            $sites = function_exists('get_sites') ? get_sites() : [];

            // sort them alphabetically, but leave the flag site at the top
            $sites_top  = [];
            $sites_rest = [];
            foreach ($sites as $site) {
                if (preg_match('/latam/i', $site->domain)) {
                    $sites_top[] = $site;
                } else {
                    $sites_rest[] = $site;
                }
            }
            usort($sites_rest, function($a, $b){
                return strcmp(strtolower($a->domain), strtolower($b->domain));
            });
            $sites = array_merge($sites_top, $sites_rest);

            foreach ($sites as $id => $site) {
                // skip the flag site
                if ($id == 0) continue;

                // populate the context
                $details = get_blog_details(['blog_id' => $site->blog_id]);
                $context['sites'][] = [
                    'blogname' => $details->blogname,
                    'url'      => sprintf('//%s', $site->domain)
                ];
            }

            // render the view
            Timber::render('region-selector-modal.twig', $context);
        }

        /**
         * social_links
         *
         * Display the social links.
         *
         * [social_links_list]
         */
        public static function social_links($atts)
        {
            $options = get_option('multisiteutils_options');

            if (empty($options['social_links'])) return;

            $out = '<ul class="social-sharing">';
            foreach ($options['social_links'] as $site => $url) {
                if (empty($url)) continue;
                $out .= sprintf(
                    '<li>' .
                        '<a href="%s" target="_blank" rel="external">' .
                            '<svg class="icon" aria-hidden="true">' .
                                '<use xlink:href="%s/defs.svg#icon-%s">' .
                            '</svg><span class="visuallyhidden">%s</span>' .
                        '</a>' .
                    '</li>',
                    $url,
                    get_template_directory_uri(),
                    $site,
                    $site
                );
            }
            $out .= '</ul>';

            return($out);
        }
    }
?>