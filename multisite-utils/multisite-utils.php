<?php
    namespace CLWP\MultisiteUtils;

    use CCL\Common\Site;
    use CCL\WordPress\AbstractPlugin;
    use \Timber\Timber as Timber;

    class Plugin extends AbstractPlugin
    {
        public static function init()
        {
            self::config(
                __FILE__,
                '5.3.1',
                __NAMESPACE__,
                __DIR__ . '/src',
                'multisite-utils'
            );

            if (is_admin()) {
                add_action('admin_init', [__NAMESPACE__ . '\\Admin', 'init']);
                add_action('admin_menu', [__NAMESPACE__ . '\\Menu', 'init']);
            }

            add_action(
                'plugins_loaded', function () {
                    if (class_exists('Timber')) {
                        Timber::$locations[] = plugin_dir_path(__FILE__) . 'views';
                    }
                }
            );

            add_action('init', [__NAMESPACE__ . '\\Shortcode', 'registerAll']);
        }
    }

    // Init the plugin
    Plugin::init();
?>
