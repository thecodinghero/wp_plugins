<?php
use CCL\Common\Site;

    class CLWP_DocumentExport {
        const TPL_HDR    = "id,title,ni number,created,modified\n";
        const TPL_ROW    = "%s,\"%s\",\"%s\",\"%s\",\"%s\"\n";
        const TPL_CSV    = 'touchpoint-documents-%s.csv';
        const PATH_CSV   = '/wp-content/uploads/temp/';
        const SQL_SELECT = "
            SELECT
                p.ID, p.post_title, p.post_date, p.post_modified,
                (
                    SELECT meta_value
                    FROM wp_postmeta
                    WHERE meta_key LIKE 'ni_number'
                    AND post_id = p.ID
                ) AS ni_number
            FROM wp_posts AS p
            WHERE p.post_type LIKE 'document'
            AND p.post_status LIKE 'publish'
        ";


        public static function init() {

            if (is_admin()) {
                add_action('admin_menu',              array('CLWP_DocumentExport', 'admin_menu'));
                add_action('admin_init',              array('CLWP_DocumentExport', 'admin_init'));
                add_action('admin_enqueue_scripts',   array('CLWP_DocumentExport', 'document_export_enqueue'));
                add_action('wp_ajax_document_export', array('CLWP_DocumentExport', 'document_export_callback'));
            }
        }

        public static function admin_init() {
            register_setting(
                'documentexport_options',
                'documentexport_options',
                array('CLWP_DocumentExport', 'options_validate')
            );
        }

        // editor panel for this plugin
        public static function admin_menu() {
            add_options_page(
                'Clorox / Document Export',
                'Document Export',
                'manage_options',
                'documentexport_options',
                array('CLWP_DocumentExport', 'admin_menu_options')
            );
        }

        // editor panel content and layout
        public static function admin_menu_options() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            printf(
                '<div class="wrap">' .
                '<h2>Clorox / Document Export</h2>' .
                '</div>' .
                '<div id="document-export-info">' .
                '<p>Click the button below to export a csv list of TouchPoint documents.' .
                '</div>' .
                '<div id="document-export-form">' .
                '<form>' .
                '<input id="document-export-button" class="button button-primary" type="button" value="Export Documents" onClick="submitdocumentexport();" />' .
                '<img id="document-export-loading" src="%s" style="display: none;" />' .
                '</form>' .
                '</div>',
                plugins_url('/spinner.gif', __FILE__)
            );
        }

        // init the document export callback
        public static function document_export_enqueue($hook) {
            wp_enqueue_script(
                'ajax-handle-document',
                plugins_url('/document_export.js', __FILE__),
                array('jquery')
            );

            wp_localize_script(
                'ajax-handle-document',
                'ajax_object',
                array(
                    'ajax_url' => admin_url('admin-ajax.php')
                )
            );
        }

        /**
         * document_export_callback
         *
         * Export the documents, create the csv, and pass
         * the path to the file to the ajax caller.
         */
        public static function document_export_callback() {
            $csv = sprintf(self::TPL_CSV, date('Ymd'));
            $out = self::TPL_HDR;
            $res = self::getDocuments();

            foreach ($res as $document) {
                // stuff the data into the row template and append the output
                $out .= sprintf(
                    self::TPL_ROW,
                    $document->ID,
                    $document->post_title,
                    $document->ni_number,
                    $document->post_date,
                    $document->post_modified
                );
            }

            // write the file
            file_put_contents(TEMP_DIRECTORY . $csv, $out);

            // return the file location
            die(Site::$config->webroot . self::PATH_CSV . $csv);
        }

        private static function getDocuments() {
            global $wpdb;
            return($wpdb->get_results(self::SQL_SELECT));
        }
    }

    CLWP_DocumentExport::init();
?>
