function submitdocumentexport() {
    var data = {
        'action': 'document_export'
    };

    jQuery('#document-export-button').hide();
    jQuery('#document-export-loading').show();

    jQuery.post(ajax_object.ajax_url, data, function(response) {
        jQuery('#document-export-loading').hide();
        jQuery('#document-export-button').show();
        if (response.length) {
            location.href = response;
        } else {
            alert('There was an error downloading documents.');
        }
    });
}
