(function($) {
    $(document).ready(function($) {
        var hidden = false;

        // toggle display between main nav and region selector
        $(".current-li, .current-li-mobile").on('click mouseover focus', function() {
            if (!$("#nav-global_menu-desktop").hasClass("show-menu") || !$("#nav-global_menu-mobile").hasClass("show-menu")) {
                $("#chevron-desktop, #chevron-mobile").removeClass("bottom");
                $("#chevron-desktop, #chevron-mobile").addClass("top");
            }
        });

        $(".current-li, .current-li-mobile, #region-list-desktop, #region-list-mobile").on('blur mouseout mouseleave focusout', function() {
            $("#chevron-desktop, #chevron-mobile").removeClass("top");
            $("#chevron-desktop, #chevron-mobile").addClass("bottom");
            $("body").trigger('click');
        });

        $("#nav-open-btn").on('click mouseover focus', function() {
            if ($("#region-list-desktop").hasClass("show-menu")) {
                $("#region-list-desktop").removeClass("show-menu");
            }
        });

        // close the region selector if you click the current item
        $('.region-select-desktop .current-li').add('.region-select-mobile .current-li-mobile').on('click touchstart focus', function(e) {
            if(e.type == 'touchstart') {
                // Don't trigger mouseenter even if they hold
                e.stopImmediatePropagation();
                // If $item is a link (<a>), don't go to said link on mobile, show menu instead
                e.preventDefault();
            }
            $('#region-list-desktop').hide();
            $('#region-list-desktop').removeClass('show-menu');

            if ($("#region-list-mobile").hasClass('show-menu')) {
                $('#region-list-mobile').hide();
                $('#region-list-mobile').removeClass('show-menu');
                $("#chevron-desktop, #chevron-mobile").removeClass("top");
                $("#chevron-desktop, #chevron-mobile").addClass("bottom");
            } else {
                $('#region-list-mobile').show();
                $('#region-list-mobile').addClass('show-menu');                
                $("#chevron-desktop, #chevron-mobile").removeClass("bottom");
                $("#chevron-desktop, #chevron-mobile").addClass("top");
            }
            hidden = true;
            setTimeout(function() {
                hidden = false;
            }, 1500);
        });

        $('#region-list-mobile .menu-item a').on('click touchend', function(e) {
            var el = $(this);
            var link = el.attr('href');
            window.location = link;
        });

        // $('.region-select-desktop .current-li').add('.region-select-mobile .current-li-mobile').on('click mouseover focus', function() {
        //     if (hidden == false) {
        //         $('#region-list-desktop').show();
        //         $('#region-list-mobile').show();
        //         $("#chevron-desktop, #chevron-mobile").removeClass("bottom");
        //         $("#chevron-desktop, #chevron-mobile").addClass("top");
        //     }
        // });
        $(document).bind("scroll", function(){
            $('#region-list-desktop, #region-list-mobile').removeClass('show-menu');
            $("#chevron-desktop, #chevron-mobile").removeClass("top");
            $("#chevron-desktop, #chevron-mobile").addClass("bottom");
        });
    });
})(jQuery);