<?php
    namespace CLWP;

    class RegionSelect
    {
        static $pluginUpdateChecker;

        public static function init()
        {
            if (is_admin()) {
                add_action('admin_menu',            array('CLWP\RegionSelect', 'admin_menu'));
                add_action('admin_init',            array('CLWP\RegionSelect', 'admin_init'));
                add_action('admin_enqueue_scripts', array('CLWP\RegionSelect', 'admin_enqueue'));
            }

            self::requirements();

            // enable the short code
            add_shortcode('regionselectbox', array('CLWP\RegionSelect', 'region_selector'));

            // load the menu toggle script
            add_action('wp_enqueue_scripts', array('CLWP\RegionSelect', 'scripts_enqueue'));
        }

        public static function requirements() {}

        public static function admin_init()
        {
            register_setting(
                'regionselect_options',
                'regionselect_options',
                array('CLWP\RegionSelect', 'options_validate')
            );

            add_settings_section(
                'regionselect_options_all',
                'Region Select Settings',
                array('CLWP\RegionSelect', 'options_desc_all'),
                'regionselect'
            );

            add_settings_field(
                'regionselect_regions',
                'Regions',
                array('CLWP\RegionSelect', 'options_field_regions'),
                'regionselect',
                'regionselect_options_all'
            );

            wp_enqueue_style('clwp_regionselect_admin_styles');
        }

        // init the admin scripts
        public static function admin_enqueue($hook)
        {
            wp_enqueue_script(
                'region-select-admin',
                plugins_url('/admin.js', __FILE__),
                array('jquery'),
                false,
                true
            );
        }

        public static function scripts_enqueue()
        {
            wp_enqueue_script(
                'region-select-lib',
                plugins_url('/lib.js', __FILE__),
                array('jquery'),
                false,
                true
            );
        }

        /**
         * options_desc_all
         *
         * Top usage instructions.
         */
        public static function options_desc_all()
        {
            print('<p>Enter the country code, url, link to be displayed on desktop, and link to be displayed on mobile.  For example:<br /><pre>AU&nbsp;&nbsp;&nbsp;https://www.kingsfordcharcoal.com.au&nbsp;&nbsp;&nbsp;Australia&nbsp;&nbsp;&nbsp;EN-AU</pre></p><p>Use the code below to display the region selector shortcode for the current page:<br /><pre>&lt;?= do_shortcode("[regionselectbox page=\'{$post->post_name}\']"); ?&gt;</pre></p>');
        }

        // editor panel for this plugin
        public static function admin_menu()
        {
            add_options_page(
                'Region Select',
                'Region Select',
                'manage_options',
                'regionselect_options',
                array('CLWP\RegionSelect', 'admin_menu_options')
            );
        }

        // editor panel content and layout
        public static function admin_menu_options()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            print(
                '<div class="wrap">' .
                '<h2>Region Select</h2>' .
                '<form method="post" action="options.php">'
            );
            settings_fields('regionselect_options');
            do_settings_sections('regionselect');
            submit_button();
        }

        public static function options_field_regions($post)
        {
            $options = get_option('regionselect_options');
            $regions = !empty($options['regions']) ? $options['regions'] : array();
            $item    = 0;

            print(
                '<table id="clx_regionselect_table">' .
                    '<tr>' .
                        '<th><label>Country Code</label></th>' .
                        '<th><label>URL</label></th>' .
                        '<th><label>Desktop Label</label></th>' .
                        '<th><label>Desktop Menu</label></th>' .
                        '<th><label>Mobile Label</label></th>' .
                        '<th><label>Mobile Menu</label></th>' .
                    '</tr>'
            );

            if (!empty($regions)) {
                foreach ($regions as $code => $row) {
                    printf(
                        '<tr>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][code]" value="%s" /></td>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][url]" value="%s" /></td>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][desktop]" value="%s" /></td>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][desktop_menu]" value="%s" /></td>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][mobile]" value="%s" /></td>' .
                            '<td><input type="text" name="regionselect_options[regions][%s][mobile_menu]" value="%s" /></td>' .
                        '</tr>',
                        $item,
                        $code,
                        $item,
                        !empty($row['url']) ? $row['url'] : null,
                        $item,
                        !empty($row['desktop']) ? $row['desktop'] : null,
                        $item,
                        !empty($row['desktop_menu']) ? $row['desktop_menu'] : null,
                        $item,
                        !empty($row['mobile']) ? $row['mobile'] : null,
                        $item,
                        !empty($row['mobile_menu']) ? $row['mobile_menu'] : null
                    );
                    $item++;
                }
            }

            print(
                '</table>' .
                '<input id="add-regionselect-number" type="text" size="2" />&nbsp;&nbsp;<input id="add-regionselect-row" type="button" value="+" />'
            );

            printf(
                '<input type="hidden" id="regionselect-count" value="%s" />',
                $item
            );
        }

        public static function options_validate($input)
        {
            $options = get_option('regionselect_options');
            $output  = array();

            if (!empty($input['regions'])) {
                foreach ($input['regions'] as $row) {
                    $output[$row['code']] = array(
                        'url'          => $row['url'],
                        'desktop'      => $row['desktop'],
                        'desktop_menu' => $row['desktop_menu'],
                        'mobile'       => $row['mobile'],
                        'mobile_menu'  => $row['mobile_menu']
                    );
                }
            }

            $options['regions'] = $output;
            return($options);
        }

        public static function region_selector($atts, $content=null)
        {
            // get the current page from the do_shortcode call
            $a = shortcode_atts(
                array('page' => null),
                $atts
            );

            // get the current site region settings
            $toggle_label_desktop = get_blog_option($site['blog_id'], 'toggle_label_desktop');
            $toggle_label_mobile  = get_blog_option($site['blog_id'], 'toggle_label_mobile');

            // get the alternate page from the other local site(s)
            $alts = \Kingsford\Theme::$ALTERNATEPAGE;
            if (empty($alts) || !is_array($alts)) $alts = array();

            // get the region settings for the external site(s)
            $options = get_option('regionselect_options');
            if (!is_array($options) || empty($options['regions'])) $options['regions'] = array();

            // init the selector for mobile and desktop
            $outDesktop = '<div class="region-select-desktop"><ul id="region-select-desktop" class="dropdown_container">';
            $outMobile  = '<div class="region-select-mobile"><ul id="region-select-mobile" class="dropdown_container">';

            // set the first option to the current site
            if (!empty($toggle_label_desktop) && !empty($toggle_label_mobile)) {
                $outDesktop .= sprintf(
                    '<li class="menu-item current-li"><span id="chevron-desktop" class="chevron bottom"></span><a href="#">%s</a>',
                    $toggle_label_desktop
                );
                $outMobile .= sprintf(
                    '<li class="menu-item current-li-mobile"><span id="chevron-mobile" class="chevron bottom"></span><a href="#">%s</a>',
                    $toggle_label_mobile
                );
            }

            // begin the list for the other sites
            $outDesktop .= '<ul id="region-list-desktop" class="menu">';
            $outMobile  .= '<ul id="region-list-mobile" class="menu">';

            // add the alternate page from the other local site(s)
            foreach ($alts as $alt) {
                $outDesktop .= sprintf(
                    '<li class="menu-item"><a href="%s" class="region-link-%s" data-code="%s">%s</a></li>',
                    $alt['url'],
                    strtolower($alt['country_code']),
                    $alt['country_code'],
                    $alt['desktop_menu']
                );

                $outMobile .= sprintf(
                    '<li class="menu-item"><a href="%s" class="region-link-%s" data-code="%s">%s</a></li>',
                    $alt['url'],
                    strtolower($alt['country_code']),
                    $alt['country_code'],
                    $alt['mobile_menu']
                );
            }

            foreach ($options['regions'] as $code => $region) {
                $outDesktop .= sprintf(
                    '<li class="menu-item"><a href="%s" class="region-link-%s" data-code="%s">%s</a></li>',
                    $region['url'],
                    strtolower($code),
                    $code,
                    $region['desktop_menu']
                );

                $outMobile .= sprintf(
                    '<li class="menu-item"><a href="%s" class="region-link-%s" data-code="%s">%s</a></li>',
                    $region['url'],
                    strtolower($code),
                    $code,
                    $region['mobile_menu']
                );
            }

            // close the current site list
            $outDesktop .= '</ul>';
            $outMobile  .= '</ul>';

            $outDesktop .= '</li>';
            $outMobile  .= '</li>';

            // close the selector
            $outDesktop .= '</ul></div>';
            $outMobile  .= '</ul></div>';

            // concat the select boxes fir hiding in css
            $out = $outDesktop . $outMobile;
            return($out);
        }
    }

    // Init the plugin
    RegionSelect::init();
?>
