(function($) {
    $(document).ready(function($) {
        $('#add-regionselect-row').click(function() {
            var index = Number($('#regionselect-count').val());
            var start = 1;

            if ($.isNumeric($('#add-regionselect-number').val())) {
                start = Number($('#add-regionselect-number').val());
            }

            for (var i = 0; i < start; i++) {
                $('#clx_regionselect_table > tbody:last').append(
                    '<tr>' +
                        '<td><input type="text" name="clorox_regionselect_options[regions][' + index + '][code]" /></td>' +
                        '<td><input type="text" name="clorox_regionselect_options[regions][' + index + '][url]" /></td>' +
                        '<td><input type="text" name="clorox_regionselect_options[regions][' + index + '][desktop]" /></td>' +
                        '<td><input type="text" name="clorox_regionselect_options[regions][' + index + '][mobile]" /></td>' +
                    '</tr>'
                );
                index++;
            }

            $('#regionselect-count').attr('value', index);
        });
    });
})(jQuery);
