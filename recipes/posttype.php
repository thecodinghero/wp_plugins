<?php
    /**
     * Custom recipe post type.
     */
    namespace CLWP\Recipes;

    use CLWP\Recipes\Recipes;

    class Recipe {

        const POST_TYPE = 'clx_recipe';
        const POST_SLUG = 'recipe';
        const GRP_SLUG  = 'recipe';


        public static function init() {
            add_action('init',       array('CLWP\Recipes\Recipe', 'registerRecipePostType'));
            add_action('admin_init', array('CLWP\Recipes\Recipe', 'adminInit'));
        }

        public static function registerRecipePostType() {
            register_post_type(self::POST_TYPE, array(
                'labels' => array(
                    'name'          => __('Recipes'),
                    'singular_name' => __('Recipe')
                ),
                'public'          => true,
                'has_archive'     => false,
                'rewrite'         => array('slug' => self::GRP_SLUG),
                'description'     => 'Post type used for all recipes',
                'menu_position'   => 8,
                'menu_icon'       => 'dashicons-carrot', // See http://melchoyce.github.io/dashicons/
                'capability_type' => 'page',
                'supports'        => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
                'taxonomies'      => array('category')
            ));

            register_taxonomy(
                'recipe',
                self::POST_TYPE,
                array(
                    'label'             => __('Type'),
                    'rewrite'           => array('slug' => self::POST_SLUG),
                    'hierarchical'      => true,
                    'show_admin_column' => true
                )
            );
        }

        /**
         * adminInit
         * 
         * Call method to add extra editors, post-save hooks.
         */
        public static function adminInit() {
            add_action('add_meta_boxes_clx_recipe', array('CLWP\Recipes\Recipe', 'addExtras'));
            add_action('save_post',                 array('CLWP\Recipes\Recipe', 'saveMetaData'));
        }

        /*
         * addExtras
         *
         * Call the render methods for the custom meta fields for this type.
         */
        public static function addExtras($post) {
            // recipe data
            add_meta_box(
                'recipe-data',
                __('Data'),
                array('CLWP\Recipes\Recipe', 'renderRecipeData'),
                self::POST_TYPE,
                'normal',
                'high'
            );

            // ingredients
            add_meta_box(
                'recipe-ingredients',
                __('Ingredients'),
                array('CLWP\Recipes\Recipe', 'renderIngredients'),
                self::POST_TYPE,
                'normal',
                'high'
            );

            // insructions
            add_meta_box(
                'recipe-instructions',
                __('Instructions'),
                array('CLWP\Recipes\Recipe', 'renderInstructions'),
                self::POST_TYPE,
                'normal',
                'high'
            );

            // details
            add_meta_box(
                'recipe-details',
                __('Details'),
                array('CLWP\Recipes\Recipe', 'renderDetails'),
                self::POST_TYPE,
                'normal',
                'high'
            );

            // tags
            add_meta_box(
                'recipe-tags',
                __('Recipe Tags'),
                array('CLWP\Recipes\Recipe', 'renderTags'),
                self::POST_TYPE,
                'normal',
                'high'
            );

            // order
            add_meta_box(
                'recipe-sort-order',
                __('Sort Order'),
                array('CLWP\Recipes\Recipe', 'renderSortOrder'),
                self::POST_TYPE,
                'normal',
                'high'
            );
        }

        /**
         * renderRecipeData
         *
         * Editor for the extra meta fields.
         */
        public static function renderRecipeData($post) {
            $data            = get_post_meta($post->ID, 'recipe_data',            true);
            $main_ingredient = get_post_meta($post->ID, 'recipe_main_ingredient', true);

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            print('<table id="clx_recipe_data_table">');
            printf(
                '<tr>' .
                    '<td><label>Main Ingredient</label></td>' .
                    '<td><input type="text" name="recipe_main_ingredient" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Category</label></td>' .
                    '<td><input type="text" name="recipe_data[category]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Servings</label></td>' .
                    '<td><input type="text" name="recipe_data[servings]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Prep Time</label></td>' .
                    '<td><input type="text" name="recipe_data[prep_time]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Cook Time</label></td>' .
                    '<td><input type="text" name="recipe_data[cook_time]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Country</label></td>' .
                    '<td><input type="text" name="recipe_data[country]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Region</label></td>' .
                    '<td><input type="text" name="recipe_data[region]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Product ID</label></td>' .
                    '<td><input type="text" name="recipe_data[product_id]" value="%s" size="25" /></td>' .
                '</tr>' .
                '<tr>' .
                    '<td><label>Category ID</label></td>' .
                    '<td><input type="text" name="recipe_data[category_id]" value="%s" size="25" /></td>' .
                '</tr>',
                !empty($main_ingredient)     ? $main_ingredient     : null,
                !empty($data['category'])    ? $data['category']    : null,
                !empty($data['servings'])    ? $data['servings']    : null,
                !empty($data['prep_time'])   ? $data['prep_time']   : null,
                !empty($data['cook_time'])   ? $data['cook_time']   : null,
                !empty($data['country'])     ? $data['country']     : null,
                !empty($data['region'])      ? $data['region']      : null,
                !empty($data['product_id'])  ? $data['product_id']  : null,
                !empty($data['category_id']) ? $data['category_id'] : null
            );
            print('</table>');
        }

        public static function renderIngredients($post) {
            $ingredients = get_post_meta($post->ID, 'recipe_ingredients', true);
            $item        = 0;

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            print(
                '<table id="clx_recipe_ingredients_table">' .
                    '<tr>' .
                        '<th><label>Set</label></th>' .
                        '<th><label>Order</label></th>' .
                        '<th><label>Amount</label></th>' .
                        '<th><label>Unit</label></th>' .
                        '<th><label>Prefix</label></th>' .
                        '<th><label>Ingredient</label></th>' .
                        '<th><label>Suffix</label></th>' .
                    '</tr>'
            );

            if (!empty($ingredients)) {
                foreach ($ingredients as $set => $list) {
                    foreach ($list as $ingredient) {
                        printf(
                            '<tr>' .
                                '<td><input type="text" name="ingredients[%s][set]" value="%s" /></td>' .            // set column
                                '<td><input type="text" name="ingredients[%s][order]" value="%s" size="2" /></td>' . // order column
                                '<td><input type="text" name="ingredients[%s][amount]" value="%s" /></td>' .         // amount column
                                '<td><input type="text" name="ingredients[%s][unit]" value="%s" /></td>' .           // unit column
                                '<td><input type="text" name="ingredients[%s][prefix]" value="%s" /></td>' .         // prefix column
                                '<td><input type="text" name="ingredients[%s][ingredient]" value="%s" /></td>' .     // ingredient column
                                '<td><input type="text" name="ingredients[%s][suffix]" value="%s" /></td>' .         // suffix column
                            '</tr>',
                            $item,
                            $set,
                            $item,
                            $ingredient['order'],
                            $item,
                            $ingredient['amount'],
                            $item,
                            $ingredient['unit'],
                            $item,
                            $ingredient['prefix'],
                            $item,
                            $ingredient['ingredient'],
                            $item,
                            $ingredient['suffix']
                        );
                        $item++;
                    }
                }
            }

            print(
                '</table>' .
                '<input id="add-ingredients-number" type="text" size="2" />&nbsp;&nbsp;<input id="add-ingredients-row" type="button" value="+" />'
            );

            printf(
                '<input type="hidden" id="ingredient-count" value="%s" />',
                $item
            );
        }

        public static function renderInstructions($post) {
            $instructions = get_post_meta($post->ID, 'recipe_instructions', true);

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            print(
                '<table id="clx_recipe_instructions_table">' .
                    '<tr>' .
                        '<th><label>Set</label></th>' .
                        '<th><label>Instruction</label></th>' .
                    '</tr>'
            );

            if (!empty($instructions)) {
                $item = 0;
                foreach ($instructions as $set => $list) {
                    foreach ($list as $instruction) {
                        printf(
                            '<tr>' .
                                '<td><input type="text" name="instructions[%s][set]" value="%s" /></td>' .        // set column
                                '<td><input type="text" name="instructions[%s][instruction]" value="%s" /></td>' . // instruction column
                            '</tr>',
                            $item,
                            $set,
                            $item,
                            $instruction
                        );
                        $item++;
                    }
                }
            }

            print(
                '</table>' .
                '<input id="add-instructions-number" type="text" size="2" />&nbsp;&nbsp;<input id="add-instructions-row" type="button" value="+" />'
            );

            printf(
                '<input type="hidden" id="instruction-count" value="%s" />',
                $item
            );
        }

        /**
         * renderDetails
         *
         * Editor for the extra meta fields.
         */
        public static function renderDetails($post) {
            $details = get_post_meta($post->ID, 'recipe_details', true);

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            print(
                '<table id="clx_recipe_details_table">' .
                    '<tr>' .
                        '<th><label>Name</label></th>' .
                        '<th><label>Value</label></th>' .
                    '</tr>'
            );

            if (!empty($details)) {
                $item = 0;
                foreach ($details as $detail) {
                    printf(
                        '<tr>' .
                            '<td><input type="text" name="details[%s][name]" value="%s" /></td>' .  // name column
                            '<td><input type="text" name="details[%s][value]" value="%s" /></td>' . // value column
                        '</tr>',
                        $item,
                        $detail['name'],
                        $item,
                        $detail['value']
                    );
                    $item++;
                }
            }

            print(
                '</table>' .
                '<input id="add-details-number" type="text" size="2" />&nbsp;&nbsp;<input id="add-details-row" type="button" value="+" />'
            );

            printf(
                '<input type="hidden" id="detail-count" value="%s" />',
                $item
            );
        }

        /**
         * renderSortOrder
         *
         * Sort order editor.
         */
        public static function renderSortOrder($post) {
            $sort_order = get_post_meta($post->ID, 'sort_order', true);

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            printf(
                '<table id="clx_recipe_sort_order_table">' .
                    '<tr>' .
                        '<td><input type="text" name="sort_order" size="25" value="%s" /></td>' .
                    '</tr>' .
                '</table>',
                !empty($sort_order) ? $sort_order : null
            );
        }

        /**
         * renderTags
         *
         * Tag editor.
         */
        public static function renderTags($post) {
            $tags = get_post_meta($post->ID, 'recipe_tags', true);
            if (!empty($tags) && is_array($tags)) $tags = implode(',', $tags);

            wp_nonce_field('clx_recipe_meta', 'clx_recipe_meta_nonce');

            printf(
                '<table id="clx_recipe_tags_table">' .
                    '<tr>' .
                        '<td><input type="text" name="recipe_tags" size="25" value="%s" /></td>' .
                    '</tr>' .
                '</table>',
                !empty($tags) ? $tags : null
            );
        }

        /*
         * saveMetaData
         *
         * Post-save hook for the new meta fields.
         */
        public static function saveMetaData($post_id) {
            // wp validataion
            if ((!isset($_POST['post_type']) || $_POST['post_type'] != self::POST_TYPE) ||
                !isset($_POST['clx_recipe_meta_nonce']) ||
                (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
                !current_user_can('edit_page', $post_id) ||
                !wp_verify_nonce($_POST['clx_recipe_meta_nonce'], 'clx_recipe_meta')) {
                return($post_id);
            }

            // data
            update_post_meta($post_id, 'recipe_main_ingredient', sanitize_text_field($_POST['recipe_main_ingredient']));
            update_post_meta($post_id, Recipes::KEY_RECIPE_DATA, $_POST['recipe_data']);

            // ingredients
            if (!empty($_POST['ingredients'])) {
                $ingredients = array();

                foreach ($_POST['ingredients'] as $ingredient) {
                    $set = !empty($ingredient['set']) ? $ingredient['set'] : null;
                    $ingredients[$set][] = $ingredient;
                }

                update_post_meta($post_id, Recipes::KEY_RECIPE_INGREDIENTS, $ingredients);
            }

            // instructions
            if (!empty($_POST['instructions'])) {
                $ingredients = array();

                foreach ($_POST['instructions'] as $instruction) {
                    $set = !empty($instruction['set']) ? $instruction['set'] : null;
                    $instructions[$set][] = $instruction['instruction'];
                }

                update_post_meta($post_id, Recipes::KEY_RECIPE_INSTRUCTIONS, $instructions);
            }

            // details
            update_post_meta($post_id, Recipes::KEY_RECIPE_DETAILS, $_POST['details']);

            // sort order
            update_post_meta($post_id, 'sort_order', sanitize_text_field($_POST['sort_order']));

            // tags
            if (!empty($_POST['recipe_tags'])) {
                $_POST['recipe_tags'] = explode(',', $_POST['recipe_tags']);
                foreach ($_POST['recipe_tags'] as &$tag) $tag = trim($tag);
                $_POST['recipe_tags'] = implode(',', $_POST['recipe_tags']);
            }
            update_post_meta($post_id, 'recipe_tags', sanitize_text_field($_POST['recipe_tags']));
        }

        /**
         * getRecipeByServiceID
         *
         * Returns the id of the first post which has a
         * recipe_service_id that matches the given id.
         *
         * @param   int  $key  recipe service id
         * @return  int        post id
         */
        public static function getRecipeByServiceID($sid=null) {
            global $wpdb;
            $res = $wpdb->get_results(
                "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = 'recipe_service_id' AND meta_value IN ({$sid})"
            );
            return($res);
        }

        /**
         * formatTitle
         *
         * Returns the properly formatted recipe title
         * for the given recipe object.
         *
         * @param   object  $recipe       single recipe object
         * @return  array   $ingredients  formatted title
         */
        public static function formatTitle(&$recipe) {
            return(str_replace(array('®','™'), array('&reg;','&trade;'), $recipe->post_title));
        }

        /**
         * formatImage
         *
         * Returns the image url for the given recipe.
         *
         * @param   object  $recipe  single recipe object
         * @return  string  $image   url of the image
         */
        public static function formatImage(&$recipe) {
            // get the recipe image upload directory
            $opt = get_option('recipes_options');
            $iud = !empty($opt['image_dir']) ? '/' . trim($opt['image_dir']) : null;
            $img = null;

            if (!empty($recipe->image)) {
                $img = "{$iud}/{$recipe->image}";
            }

            return($img);
        }

        /**
         * formatTime
         *
         * Returns given time in minutes into quarter hour
         * increments if it's more than 60 minutes.
         *
         * @param   int     $minutes  single recipe object
         * @return  string            formatted time
         */
        public static function formatTime($minutes=null) {
        	if (!empty($minutes)) {
        	    if ($minutes > 59) {
            		$hours = $minutes / 60;
            		$hourByQuarter = ceil($hours * 4) / 4;

            		$timeString = strval($hourByQuarter);

            		// get decimal point so we can correctly output readable amount
            		$decimalAt = strpos($timeString, '.');
            		if (!$decimalAt) {
            			$decimalAt = strlen($timeString);
            		}

            		// how long is the string
            		$decimals = substr($timeString, ($decimalAt + 1));
            		$number = substr($timeString, 0, $decimalAt);

            		switch ($decimals) {
            			case '25':
            				$ext = ' &frac14;';
            				break;
            			case '5':
            				$ext = ' &frac12;';
            				break;
            			case '75':
            				$ext = ' &frac34;';
            				break;
            			default:
            				$ext = null;
            		}

            		$label = ($number == 1 && empty($ext)) ? ' hour' : ' hours';

            		return("{$number}{$ext}{$label}");
        	    } else {
                	return("{$minutes} minutes");
        	    }
        	}
        	return(false);
        }
    }

    // call it
    Recipe::init();
?>