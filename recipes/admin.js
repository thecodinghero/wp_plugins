(function($) {
    $(document).ready(function($) {
        $('#add-ingredients-row').click(function() {
            var index = Number($('#ingredient-count').val());
            var start = 1;

            if ($.isNumeric($('#add-ingredients-number').val())) {
                start = Number($('#add-ingredients-number').val());
            }

            for (var i = 0; i < start; i++) {
                $('#clx_recipe_ingredients_table > tbody:last').append(
                    '<tr>' +
                        '<td><input type="text" name="ingredients[' + index + '][set]" /></td>' +            // set column
                        '<td><input type="text" name="ingredients[' + index + '][order]" size="2" /></td>' + // order column
                        '<td><input type="text" name="ingredients[' + index + '][amount]" /></td>' +         // amount column
                        '<td><input type="text" name="ingredients[' + index + '][unit]" /></td>' +           // unit column
                        '<td><input type="text" name="ingredients[' + index + '][prefix]" /></td>' +         // prefix column
                        '<td><input type="text" name="ingredients[' + index + '][ingredient]" /></td>' +     // ingredient column
                        '<td><input type="text" name="ingredients[' + index + '][suffix]" /></td>' +         // suffix column
                    '</tr>'
                );
                index++;
            }

            $('#ingredient-count').attr('value', index);
        });

        $('#add-instructions-row').click(function() {
            var index = Number($('#instruction-count').val());
            var start = 1;

            if ($.isNumeric($('#add-instructions-number').val())) {
                start = Number($('#add-instructions-number').val());
            }

            for (var i = 0; i < start; i++) {
                $('#clx_recipe_instructions_table > tbody:last').append(
                    '<tr>' +
                        '<td><input type="text" name="instructions[' + index + '][set]" /></td>' +         // set column
                        '<td><input type="text" name="instructions[' + index + '][instruction]" /></td>' + // instruction column
                    '</tr>'
                );
                index++;
            }

            $('#instruction-count').attr('value', index);
        });

        $('#add-details-row').click(function() {
            var index = Number($('#detail-count').val());
            var start = 1;

            if ($.isNumeric($('#add-details-number').val())) {
                start = Number($('#add-details-number').val());
            }

            for (var i = 0; i < start; i++) {
                $('#clx_recipe_details_table > tbody:last').append(
                    '<tr>' +
                        '<td><input type="text" name="details[' + index + '][name]" /></td>' +  // name column
                        '<td><input type="text" name="details[' + index + '][value]" /></td>' + // value column
                    '</tr>'
                );
                index++;
            }

            $('#detail-count').attr('value', index);
        });
    });
})(jQuery);
