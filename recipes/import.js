function submitImport() {
    var data = {
        'action': 'import'
    };

    jQuery('#import-button').hide();
    jQuery('#import-loading').show();

    jQuery.post(ajax_object.ajax_url, data, function(response) {
        jQuery('#import-loading').hide();
        jQuery('#import-button').show();
        console.log(response);
    });
}
