function submitExport() {
    var data = {
        'action': 'export'
    };

    jQuery('#export-button').hide();
    jQuery('#export-loading').show();

    jQuery.post(ajax_object.ajax_url, data, function(response) {
        jQuery('#export-loading').hide();
        jQuery('#export-button').show();
        console.log(response);
        if (response.length) {
            location.href = response;
        } else {
            alert('There was an error downloading recipes.');
        }
    });
}
