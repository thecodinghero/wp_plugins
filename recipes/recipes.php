<?php
    namespace CLWP\Recipes;

    use CCL\Common\Site;

    class Recipes {
        const DEF_RESULTS_LIMIT          = 10;

        const KEY_RECIPE_DATA            = 'recipe_data';
        const KEY_RECIPE_SERVICE_ID      = 'recipe_service_id';
        const KEY_RECIPE_INGREDIENTS     = 'recipe_ingredients';
        const KEY_RECIPE_INSTRUCTIONS    = 'recipe_instructions';
        const KEY_RECIPE_DETAILS         = 'recipe_details';
        const KEY_RECIPE_TAGS            = 'recipe_tags';
        const KEY_RECIPE_MAIN_INGREDIENT = 'recipe_main_ingredient';

        const ERROR_RECUPE_USER          = 'invalid recipe user';
        const ERROR_RECIPE_LIST          = 'error getting recipe list';
        const ERROR_RECIPE_DATA          = 'error getting recipe data';
        const ERROR_RECIPE_NONE          = 'no recipe data was found';

        const PARAM_RECIPE_ID            = 'recipeId';
        const PARAM_MAIN_INGREDIENT      = 'mainIngredient';

        const RESULT_KEY_DATA            = 'recipes';
        const RESULT_KEY_COUNT           = 'total';
        const RESULT_KEY_TIMER           = 'elapsed';

        // services
        const SVC_SEARCH                 = '/search';
        const SVC_DETAIL                 = '/detail';

        // search params
        const PRM_RECIPEID               = 'recipeId';
        const PRM_TERM                   = 'term';
        const PRM_CATEGORY               = 'category';
        const PRM_DETAIL                 = 'detail';
        const PRM_LANGUAGE               = 'language';
        const PRM_MAININGREDIENT         = 'mainIngredient';
        const PRM_NAME                   = 'name';
        const PRM_SYNDICATION            = 'syndication';
        const PRM_TAG                    = 'tag';
        const PRM_TITLE                  = 'title';
        const PRM_TOTALTIME              = 'totalTime';
        const PRM_USER                   = 'user';

        // list view param
        const PRM_LIST_VIEW              = 'listView';

        // pagination params
        const PRM_PAGE                   = 'page';
        const PRM_LIMIT                  = 'limit';

        // export vars
        const FIL_EXPORT                 = 'kingsford-recipes.zip';
        const MAX_EXPORT                 = 1000;
        const TPL_EXPORT                 = "%s\n\nTAGS\n%s\n\nMAIN INGREDIENT\n%s\n\n\nINGREDIENTS\n%s\n\nINSTRUCTIONS\n%s\n\n\nDETAILS\n%s";
        const DIR_EXPORT                 = '/data/shared/uploads/temp/';
        const PTH_EXPORT                 = '/wp-content/uploads/temp/';

        // params that will be used by the service
        private static $supported_params = array(
            self::PRM_RECIPEID,
            self::PRM_TERM,
            self::PRM_CATEGORY,
            self::PRM_DETAIL,
            self::PRM_NAME,
            self::PRM_LANGUAGE,
            self::PRM_MAININGREDIENT,
            self::PRM_TAG,
            self::PRM_TOTALTIME,
            self::PRM_USER,
            self::PRM_PAGE,
            self::PRM_LIMIT,
            self::PRM_LIST_VIEW
        );


        public static function init() {
            if (is_admin()) {
                add_action('admin_menu',            array('CLWP\Recipes\Recipes', 'admin_menu'));
                add_action('admin_init',            array('CLWP\Recipes\Recipes', 'admin_init'));
                add_action('admin_enqueue_scripts', array('CLWP\Recipes\Recipes', 'admin_enqueue'));
                add_action('wp_ajax_import',        array('CLWP\Recipes\Recipes', 'import_callback'));
                add_action('wp_ajax_export',        array('CLWP\Recipes\Recipes', 'export_callback'));
            }

            self::requirements();
        }

        public static function requirements() {
            require_once('posttype.php');
        }

        public static function admin_init() {
            register_setting(
                'recipes_options',
                'recipes_options',
                array('CLWP\Recipes\Recipes', 'options_validate')
            );

            add_settings_section(
                'recipes_options_all',
                'Recipe Settings',
                array('CLWP\Recipes\Recipes', 'options_desc_all'),
                'recipes'
            );

            add_settings_field(
                'recipes_user',
                'Recipe Service User',
                array('CLWP\Recipes\Recipes', 'options_field_user'),
                'recipes',
                'recipes_options_all'
            );

            add_settings_field(
                'recipes_per_page',
                'Recipes Per Page',
                array('CLWP\Recipes\Recipes', 'options_field_per_page'),
                'recipes',
                'recipes_options_all'
            );

            add_settings_field(
                'recipes_image_dir',
                'Image Upload Directory',
                array('CLWP\Recipes\Recipes', 'options_field_image_dir'),
                'recipes',
                'recipes_options_all'
            );

            add_settings_field(
                'recipes_recipe_list',
                'Recipe List',
                array('CLWP\Recipes\Recipes', 'options_field_recipe_list'),
                'recipes',
                'recipes_options_all'
            );

            wp_register_style('clwp_recipes_admin_styles', plugins_url('/admin.css', __FILE__));
            wp_enqueue_style('clwp_recipes_admin_styles');
        }

        /**
         * options_desc_all
         *
         * Top usage instructions.
         */
        public static function options_desc_all() {
            print("<p>Use the fields below to enter the Recipe Service user for this site.  You can also enter a list of recipe ID's to specify which recipes to import.</p>");
        }

        // editor panel for this plugin
        public static function admin_menu() {
            add_options_page(
                'Recipes',
                'Recipes',
                'manage_options',
                'recipes_options',
                array('CLWP\Recipes\Recipes', 'admin_menu_options')
            );
        }

        // editor panel content and layout
        public static function admin_menu_options() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            print(
                '<div class="wrap">' .
                '<h2>Recipes</h2>' .
                '<form method="post" action="options.php">'
            );
            settings_fields('recipes_options');
            do_settings_sections('recipes');
            submit_button();

            // import controls
            printf(
                '</form>' .
                '</div>' .
                '<div id="recipe-import-info">' .
                '<p>Click the import button below to import recipes from the Recipe Service.  Existing recipes and their content will be updated/overwritten.</p>' .
                '</div>' .
                '<div id="recipe-import-form">' .
                '<form>' .
                '<input id="import-button" class="button button-primary" type="button" value="Import Recipes" onClick="submitImport();" />' .
                '<img id="import-loading" src="%s" style="display: none;" />' .
                '</form>' .
                '</div>',
                plugins_url('/spinner.gif', __FILE__)
            );

            // export controls
            printf(
                '<br />' .
                '<div id="recipe-export-info">' .
                '<p>Click the export button below to export recipes to a csv.</p>' .
                '</div>' .
                '<div id="recipe-export-form">' .
                '<form>' .
                '<input id="export-button" class="button button-primary" type="button" value="Export Recipes" onClick="submitExport();" />' .
                '<img id="export-loading" src="%s" style="display: none;" />' .
                '</form>' .
                '</div>',
                plugins_url('/spinner.gif', __FILE__)
            );
        }

        // recipe service user name
        public static function options_field_user() {
            $options = get_option('recipes_options');
            printf(
                '<input name="recipes_options[user]" value="%s" size="20"><br>',
                !empty($options['user']) ? $options['user'] : null
            );
        }

        // number of results per page
        public static function options_field_per_page() {
            $options = get_option('recipes_options');
            printf(
                '<input name="recipes_options[per_page]" value="%s" size="20"><br>',
                !empty($options['per_page']) ? $options['per_page'] : null
            );
        }

        // uploads path
        public static function options_field_image_dir() {
            $options = get_option('recipes_options');
            printf(
                '<input name="recipes_options[image_dir]" value="%s" size="20"><br>',
                !empty($options['image_dir']) ? $options['image_dir'] : null
            );
        }

        // override list of recipes (optional)
        public static function options_field_recipe_list() {
            $options = get_option('recipes_options');
            printf(
                '<textarea name="recipes_options[recipe_list]" cols="40" rows="6">%s</textarea><br>',
                !empty($options['recipe_list']) ? $options['recipe_list'] : null
            );
        }

        public static function options_validate($input) {
            $options = get_option('recipes_options');
            if (!empty($input['user']))        $options['user']        = $input['user'];
            if (!empty($input['per_page']))    $options['per_page']    = $input['per_page'];
            if (!empty($input['image_dir']))   $options['image_dir']   = $input['image_dir'];
            if (!empty($input['recipe_list'])) $options['recipe_list'] = $input['recipe_list'];
            return($options);
        }

        // init the admin scripts
        public static function admin_enqueue($hook) {
            wp_enqueue_script(
                'admin-handle',
                plugins_url('/admin.js', __FILE__),
                array('jquery'),
                false,
                true
            );

            wp_enqueue_script(
                'import-handle',
                plugins_url('/import.js', __FILE__),
                array('jquery')
            );

            wp_localize_script(
                'import-handle',
                'ajax_object',
                array(
                    'ajax_url' => admin_url('admin-ajax.php')
                )
            );

            wp_enqueue_script(
                'export-handle',
                plugins_url('/export.js', __FILE__),
                array('jquery')
            );
        }

        /**
         *                  ___           ___           ___           ___           ___
         *      ___        /\__\         /\  \         /\  \         /\  \         /\  \
         *     /\  \      /::|  |       /::\  \       /::\  \       /::\  \        \:\  \
         *     \:\  \    /:|:|  |      /:/\:\  \     /:/\:\  \     /:/\:\  \        \:\  \
         *     /::\__\  /:/|:|__|__   /::\~\:\  \   /:/  \:\  \   /::\~\:\  \       /::\  \
         *  __/:/\/__/ /:/ |::::\__\ /:/\:\ \:\__\ /:/__/ \:\__\ /:/\:\ \:\__\     /:/\:\__\
         * /\/:/  /    \/__/~~/:/  / \/__\:\/:/  / \:\  \ /:/  / \/_|::\/:/  /    /:/  \/__/
         * \::/__/           /:/  /       \::/  /   \:\  /:/  /     |:|::/  /    /:/  /
         *  \:\__\          /:/  /         \/__/     \:\/:/  /      |:|\/__/     \/__/
         *   \/__/         /:/  /                     \::/  /       |:|  |
         *                 \/__/                       \/__/         \|__|
         *
         *
         * Import methods for getting data from the recipe service and saving it locally.
         *
         */

        /**
         * import_callback
         *
         * Does all the heavy lifting to import all the recipes
         * from the service and store them locally.
         */
        public static function import_callback() {
            // get the recipe ids and service user
            $opt = get_option('recipes_options');
            $usr = !empty($opt['user'])        ? $opt['user']              : null;
            $ids = !empty($opt['recipe_list']) ? trim($opt['recipe_list']) : null;
            $ctr = 0;

            // init the service params
            $p = array('limit' => 100);
            if (!empty($ids)) {
                $p['user']     = null;
                $p['recipeId'] = $ids;
            } else if (!empty($usr)) {
                $p['user'] = $usr;
            }

            // get the recipe data from the service
            $res = self::getRecipeFromService($p);
            if (empty($res)) die(self::ERROR_RECIPE_DATA);

            // create a post for each recipe if it doesn't already exist
            if (empty($res->results)) die(self::ERROR_RECIPE_NONE);
            foreach ($res->results as &$recipe) {
                // create or update the recipe post
                $post_id = Recipe::getRecipeByServiceID($recipe->id);
                $post_id = (!empty($post_id[0]) && !empty($post_id[0]->post_id)) ? $post_id[0]->post_id : null;
                $post_id = wp_insert_post(
                    array(
                        'ID'          => $post_id,
                        'post_title'  => $recipe->title,
                        'post_status' => 'publish',
                        'post_type'   => Recipe::POST_TYPE
                    )
                );

                // set the service id meta
                update_post_meta($post_id, self::KEY_RECIPE_SERVICE_ID, $recipe->id);

                /*
                 * Get and add the recipe image
                 *
                 * Not working on staging and prod because of
                 * the funky load balancer issues.  Needs to
                 * be rewritten to use a custom method using
                 * CURL workarounds instead of media_side_load
                if (!empty($recipe->media) && is_array($recipe->media)) {
                    $image = CLWP\Recipes\Recipes::extractImage($recipe);
                    unset($recipe->media);

                    // delete the old one
                    $att = self::getAttachment($post_id);
                    if (!empty($att[0]) && !empty($att[0]->ID)) wp_delete_attachment($att[0]->ID, true);

                    // side load the image and set it as featured
                    $msi = media_sideload_image($image, $post_id, $recipe->title);
                    $att = self::getAttachment($post_id);
                    if (!empty($att[0]) && !empty($att[0]->ID)) set_post_thumbnail($post_id, $att[0]->ID);
                }
                */

                // get and add the ingredients
                if (!empty($recipe->ingredients) && is_array($recipe->ingredients)) {
                    $ingredients = CLWP\Recipes\Recipes::extractIngredients($recipe);
                    unset($recipe->ingredients);
                    update_post_meta($post_id, self::KEY_RECIPE_INGREDIENTS, $ingredients);
                }

                // get and add the instructions
                if (!empty($recipe->instructions) && is_array($recipe->instructions)) {
                    $instructions = CLWP\Recipes\Recipes::extractInstructions($recipe);
                    unset($recipe->instructions);
                    update_post_meta($post_id, self::KEY_RECIPE_INSTRUCTIONS, $instructions);
                }

                // get and add the details
                if (!empty($recipe->details) && is_array($recipe->details)) {
                    $details = CLWP\Recipes\Recipes::extractDetails($recipe);
                    unset($recipe->details);
                    update_post_meta($post_id, self::KEY_RECIPE_DETAILS, $details);
                }

                // set the tags
                if (!empty($recipe->tags)) {
                    update_post_meta($post_id, self::KEY_RECIPE_TAGS, implode(',', $recipe->tags));
                    unset($recipe->tags);
                }

                // set the main ingredient
                if (!empty($recipe->main_ingredient)) update_post_meta($post_id, self::KEY_RECIPE_MAIN_INGREDIENT, $recipe->main_ingredient);

                // remove the recipe media
                if (!empty($recipe->media)) {
                    unset($recipe->media);
                }

                // save the leftovers in the data key
                if (!empty($recipe)) {
                    $data = self::extractData($recipe);
                    update_post_meta($post_id, self::KEY_RECIPE_DATA, $data);
                    unset($recipe);
                }

                $ctr++;
            }

            // finish up and report the results
            die("{$ctr} recipes imported");
        }

        /**
         * getRecipeFromService
         *
         * Standard recipe service call.  This is an adaptation
         * of the old CCL\Recipe\Service::search()
         *
         * @param   string  $params  the search params
         * @return  object           recipe results
         */
        public static function getRecipeFromService(&$params=array()) {
            $p = array();
            if (is_array($params) && array_key_exists('user', $params) && empty($params['user'])) {
                unset($params['user']);
            } else {
                $opt = get_option('recipes_options');
                if (empty($opt['user'])) die(self::ERROR_RECIPE_LIST);
                $p = array('user' => $opt['user']);
            }
            if (!empty($params) && is_array($params)) $p = array_merge($params, $p);
            return(json_decode(self::service($p)));
        }

        /**
         * service
         *
         * Main entry point for recipe search.  Makes the API call,
         * caches the images, and returns the data.
         *
         * @param   array   $params  list of search params
         * @return  string  $data    json encoded recipe data
         */
        private static function service(&$params=array()) {
            $data = self::requestData(self::SVC_SEARCH, $params);
            return($data);
        }

        /**
         * requestData
         *
         * Makes the call to the API.
         *
         * @param   string  $service   which service to call
         * @param   array   $params    list of search params
         * @return  string  $response  json encoded api response
         */
        private static function requestData($service, &$params=array()) {
            // build the url
            $host = RDB_API_HOST;
            $url  = RDB_API_URL . $service . self::createQueryString($params);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            // add the http header for staging and prod
            if ($host) {
                $host = preg_replace('/^http(s)?:\/\//', '', RDB_API_HOST);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: {$host}"));
            }

            // the usual stuff plus turn off sslcert validator
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT,        10);

            // make the call
            $response = curl_exec($ch);
            curl_close($ch);

            return($response);
        }

        /**
         * createQueryString
         *
         * Builds the url to make the api call
         *
         * @param   array   $params  list of search params
         * @return  string  $query   the api url
         */
        private static function createQueryString(&$params=array()) {
            $query = '?';
            $plist = array();

            // create key value pairs
            foreach ($params as $param => $value) {
                if (in_array($param, self::$supported_params)) {
                    $name  = urlencode($param);
                    $value = urlencode($value);
                    $plist[] = "{$name}={$value}";
                }
            }

            // create flat &key=value string
            $query .= implode('&', $plist);

            return($query);
        }

        /**
         *  _______      ___    ___ ________  ________  ________  _________
         * |\  ___ \    |\  \  /  /|\   __  \|\   __  \|\   __  \|\___   ___\
         * \ \   __/|   \ \  \/  / | \  \|\  \ \  \|\  \ \  \|\  \|___ \  \_|
         *  \ \  \_|/__  \ \    / / \ \   ____\ \  \\\  \ \   _  _\   \ \  \
         *   \ \  \_|\ \  /     \/   \ \  \___|\ \  \\\  \ \  \\  \|   \ \  \
         *    \ \_______\/  /\   \    \ \__\    \ \_______\ \__\\ _\    \ \__\
         *     \|_______/__/ /\ __\    \|__|     \|_______|\|__|\|__|    \|__|
         *              |__|/ \|__|
         *
         *
         * Export recipes to csv format.
         *
         */

        /**
         * export_callback
         *
         * Get all the recipe data and dump it to a csv.
         */
        public static function export_callback() {
            $params = array(
                self::PRM_LIMIT => self::MAX_EXPORT
            );

            $res = self::getRecipes($params);
            if (!empty($res)) {
                // list of files to zip up
                $files = array();

                foreach ($res as $recipe) {
                    // format the ingredients
                    $ingredients    = array();
                    $rawingredients = unserialize($recipe->ingredients);
                    foreach ($rawingredients as $name => $set) {
                        if (empty($name)) $name = 'MAIN';
                        $data = "{$name}\n";
                        foreach ($set as $item) {
                            if (empty($item['ingredient'])) continue;
                            $data .= "{$item['amount']} {$item['unit']} {$item['prefix']} {$item['ingredient']} {$item['suffix']}\n";
                        }
                        $ingredients[] = $data;
                    }
                    $ingredients = implode("\n", $ingredients);

                    // format the instructions
                    $instructions    = array();
                    $rawinstructions = unserialize($recipe->instructions);
                    foreach ($rawinstructions as $name => $set) {
                        if (empty($name)) $name = 'MAIN';
                        $set = implode("\n\n", $set);
                        $instructions[] = "{$name}\n\n{$set}";
                    }
                    $instructions = implode("\n", $instructions);

                    // format the deatils
                    $details = array();
                    if (!empty($recipe->details)) {
                        $rawdetails = unserialize($recipe->details);
                        foreach ($rawdetails as $detail) {
                            $details[] = "{$detail['name']}: {$detail['value']}\n";
                        }
                    }
                    $details = implode("\n", $details);

                    // put it all in a file
                    $out = sprintf(
                        self::TPL_EXPORT,
                        $recipe->post_title,
                        $recipe->tags,
                        $recipe->main_ingredient,
                        $ingredients,
                        $instructions,
                        $details
                    );

                    $file = Site::$config->docroot . self::DIR_EXPORT . $recipe->post_name . '.txt';
                    if (file_put_contents($file, $out)) $files[] = $file;
                }

                // zip the files and serve it up
                if (self::zipFiles($files)) {
                    $zip = Site::$config->webroot . self::PTH_EXPORT . self::FIL_EXPORT;
                    die($zip);
                }
            }

            // how'd we get here?!
            die('There was an error exporting recipes.');
        }

        /**
         *      _______. _______     ___      .______        ______  __    __
         *     /       ||   ____|   /   \     |   _  \      /      ||  |  |  |
         *    |   (----`|  |__     /  ^  \    |  |_)  |    |  ,----'|  |__|  |
         *     \   \    |   __|   /  /_\  \   |      /     |  |     |   __   |
         * .----)   |   |  |____ /  _____  \  |  |\  \----.|  `----.|  |  |  |
         * |_______/    |_______/__/     \__\ | _| `._____| \______||__|  |__|
         *
         *
         * Find them recipes, yo!
         *
         */

        /**
         * search
         *
         * Main entry point for post and pages.  Searches for
         * recipes based on the given parameters.
         *
         * @param   string  $params  the search params
         * @return  array            array containing the total
         *                           number of results, elapsed
         *                           time, and an array of
         *                           recipe objects.
         */
        public static function search($params=null) {
            global $wpdb;
            $result = array();
            $count  = 0;
            $timer  = microtime(true);

            if (empty($params[self::PRM_RECIPEID])) {
                $res = self::searchRecipes($params);
                if (!empty($res) && !self::emptyArray($res)) {
                    $count = count($res);
                    $params[self::PARAM_RECIPE_ID] = implode(',', $res);
                }
            }

            // get the recipes and unserialize the array fields
            $res = self::getRecipes($params);
            if (!empty($res)) {
                foreach ($res as &$recipe) {
                    $recipe->data         = unserialize($recipe->data);
                    $recipe->ingredients  = unserialize($recipe->ingredients);
                    $recipe->instructions = unserialize($recipe->instructions);
                    $recipe->details      = unserialize($recipe->details);
                    $recipe->tags         = !empty($recipe->tags) ? explode(',', $recipe->tags) : null;
                    $result[self::RESULT_KEY_DATA][$recipe->ID] = $recipe;
                }
            }
            unset($res);

            // set the results count
            $result[self::RESULT_KEY_COUNT] = $count;

            // set the elapsed times
            $timer = microtime(true) - $timer;
            $result[self::RESULT_KEY_TIMER] = $timer;

            return($result);
        }

        /**
         * searchRecipes
         *
         * Get the recipe id's for recipes matching the search params.
         *
         * @param   string  $params   the serach params
         * @return  array   $results  array of recipe id's
         */
        private static function searchRecipes($params=null) {
            global $wpdb;
            $tbl_prefix = \Kingsford\Theme::$LOCALIZATION['table_prefix'];

            $results = array();

            // base query
            $sql = sprintf("
                SELECT p.ID
                FROM wp{$tbl_prefix}_posts AS p
                WHERE p.post_type = '%s'
                AND p.post_status = 'publish'",
                Recipe::POST_TYPE
            );

            // filter by main ingredient
            if (!empty($params[self::PRM_MAININGREDIENT])) {
                $sql .= sprintf("
                    AND p.ID IN (
                        SELECT post_id
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = '%s'
                        AND meta_value = '%s'
                    )",
                    self::KEY_RECIPE_MAIN_INGREDIENT,
                    $params[self::PRM_MAININGREDIENT]
                );
            }

            // filter by category
            if (!empty($params[self::PRM_CATEGORY])) {
                $sql .= sprintf("
                    AND p.ID IN (
                        SELECT r.object_id
                        FROM wp{$tbl_prefix}_term_relationships AS r
                        LEFT JOIN wp{$tbl_prefix}_terms AS t
                        ON t.term_id = r.term_taxonomy_id
                        LEFT JOIN wp{$tbl_prefix}_term_taxonomy AS x
                        ON x.term_id = t.term_id
                        WHERE x.taxonomy = 'category'
                        AND t.slug = '%s'
                    )",
                    $params[self::PRM_CATEGORY]
                );
            }

            // turn the results in a simple array and clean up the mess
            $res = $wpdb->get_results($sql);
            if (!empty($res)) {
                foreach ($res as $obj) $results[] = $obj->ID;
            }
            unset($res);

            return($results);
        }

        /**
         * getRecipes
         *
         * Gets the recipe data for the given recipe id's.
         *
         * @param   string  $params  the search params (typicall id's)
         * @return  array            array of recipe data
         */
        private static function getRecipes($params=null) {
            global $wpdb;
            $tbl_prefix = \Kingsford\Theme::$LOCALIZATION['table_prefix'];

            $opt = get_option('recipes_options');

            // get all recipe posts
            $sql = sprintf("
                SELECT
                    p.ID,
                    p.post_title,
                    p.post_name,
                    p.post_content,
                    p.post_type,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_main_ingredient'
                        AND post_id = p.ID
                    ) AS main_ingredient,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE post_id = (
                            SELECT meta_value
                            FROM wp{$tbl_prefix}_postmeta
                            WHERE meta_key = '_thumbnail_id'
                            AND post_id = p.ID
                        )
                        AND meta_key = '_wp_attached_file'
                    ) AS image,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_data'
                        AND post_id = p.ID
                    ) AS data,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_ingredients'
                        AND post_id = p.ID
                    ) AS ingredients,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_instructions'
                        AND post_id = p.ID
                    ) AS instructions,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_details'
                        AND post_id = p.ID
                    ) AS details,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'recipe_tags'
                        AND post_id = p.ID
                    ) AS tags,
                    (
                        SELECT meta_value
                        FROM wp{$tbl_prefix}_postmeta
                        WHERE meta_key = 'sort_order'
                        AND post_id = p.ID
                    ) AS sort_order
                FROM wp{$tbl_prefix}_posts AS p
                WHERE p.post_type = '%s'
                AND p.post_status = 'publish'",
                Recipe::POST_TYPE
            );

            // filter by recipe id
            if (!empty($params[self::PRM_RECIPEID])) {
                $sql .= sprintf("
                    AND p.ID IN (%s)",
                    $params[self::PRM_RECIPEID]
                );
            }

            // pagination
            $limit = !empty($params[self::PRM_LIMIT]) ?
                $params[self::PRM_LIMIT] : (
                !empty($opt['per_page']) ?
                    $opt['per_page'] :
                    self::DEF_RESULTS_LIMIT
                );
            $page   = !empty($params['page'])  ? $params['page']  : 1;
            $offset = ($page - 1) * $limit;
            $sql   .= sprintf(
                'ORDER BY sort_order ASC LIMIT %d,%d',
                $offset,
                $limit
            );

            return($wpdb->get_results($sql));
        }

        /**
         * ____________________________________/\\\\\\_________________
         *  ___________________________________\////\\\_________________
         *   ___________________/\\\_______/\\\____\/\\\_________________
         *    __/\\\____/\\\__/\\\\\\\\\\\_\///_____\/\\\_____/\\\\\\\\\\_
         *     _\/\\\___\/\\\_\////\\\////___/\\\____\/\\\____\/\\\//////__
         *      _\/\\\___\/\\\____\/\\\______\/\\\____\/\\\____\/\\\\\\\\\\_
         *       _\/\\\___\/\\\____\/\\\_/\\__\/\\\____\/\\\____\////////\\\_
         *        _\//\\\\\\\\\_____\//\\\\\___\/\\\__/\\\\\\\\\__/\\\\\\\\\\_
         *         __\/////////_______\/////____\///__\/////////__\//////////__
         *
         *
         * Helper methods for extracting data and such.
         *
         */

        /**
         * extractIngredients
         *
         * Returns the properly formatted ingredients list
         * for the given recipe object.
         *
         * @param   object  $recipe       single recipe object
         * @return  array   $ingredients  list of ingredients
         */
        public static function extractIngredients(&$recipe) {
            $ingredients = array();
            foreach ($recipe->ingredients as $ingredient) {
                if (!empty($ingredient->descriptor)) {
                    $descriptors = array();
                    foreach ($ingredient->descriptor as $descriptor) {
                        $descriptors[$descriptor->placement][$descriptor->order] = $descriptor->value;
                    }
                }

                if (!empty($descriptors['start'])) {
                    $descriptors['start'] = implode(', ', $descriptors['start']);
                } else {
                    $descriptors['start'] = null;
                }

                if (!empty($descriptors['end'])) {
                    $descriptors['end'] = implode(', ', $descriptors['end']);
                } else {
                    $descriptors['end'] = null;
                }

                $set = $ingredient->set;
                if (!isset($ingredientSets[$set])) {
                    $ingredientSets[$set] = array();
                }

                $ingredients[$set][] = array(
                    'order'      => $ingredient->order,
                    'amount'     => $ingredient->amount,
                    'unit'       => $ingredient->unit,
                    'prefix'     => $descriptors['start'],
                    'ingredient' => $ingredient->value,
                    'suffix'     => $descriptors['end']
                );
            }

            return($ingredients);
        }

        /**
         * extractInstructions
         *
         * Returns the properly formatted instructions
         * for the given recipe object.
         *
         * @param   object $recipe        single recipe object
         * @return  array  $instructions  list of instructions
         */
        public static function extractInstructions(&$recipe) {
            $instructions = array();
            foreach ($recipe->instructions as $step) {
                $set = $step->set;
                if (!isset($instructions[$set])) {
                    $instructions[$set] = array();
                }
                $instructions[$set][] = $step->value;
            }
            return($instructions);
        }

        /**
         * extractDetails
         *
         * Returns the properly formatted details
         * for the given recipe object.
         *
         * @param   object $recipe        single recipe object
         * @return  array  $instructions  list of details
         */
        public static function extractDetails(&$recipe) {
            $details = array();
            foreach ($recipe->details as $detail) {
                $details[] = array(
                    'name'  => $detail->name,
                    'value' => $detail->value
                );
            }
            return($details);
        }

        /**
         * extractData
         *
         * Returns the properly formatted data
         * for the given recipe object.
         *
         * @param   object $recipe        single recipe object
         * @return  array  $instructions  list of data
         */
        public static function extractData(&$recipe) {
            $data = array();
            foreach ($recipe as $key => $value) {
                $data[$key] = $value;
            }
            return($data);
        }

        /**
         * extractImage
         *
         * Returns the image url for the given recipe.
         *
         * @param   object  $recipe  single recipe object
         * @return  string  $image   url of the image
         */
        public static function extractImage(&$recipe) {
            $image = null;
            foreach ($recipe->media as $media) {
                if ($media->type == 'image') {// && strtotime($media->rights_end) >= time()) {
                    $image = $media->file;
                }
            }
            return($image);
        }

        /**
         * getAttachment
         *
         * Utility method to get a post's attachments.
         *
         * @param   string  $post_id     the post id
         * @return  array   $attachment  the atachments
         */
        public static function getAttachment($post_id=null) {
            $attachment = get_posts(
                array(
                    'numberposts'    => '1',
                    'post_parent'    => $post_id,
                    'post_type'      => 'attachment',
                    'post_mime_type' => 'image',
                    'order'          => 'ASC'
                )
            );
            return($attachment);
        }

        /**
         * emptyArray
         *
         * Check each element of the array to see if it's empty
         *
         * @param   array  $data  array to check
         * @return  bool
         */
        public static function emptyArray($data=null) {
            $empty = false;
            if (count($data)) {
                foreach ($data as $key => $value) {
                    if (is_array($value)) {
                        $empty = self::emptyArray($value);
                    } else {
                        $empty = ($value === null || $value === '');
                    }
                }
            } else {
                return(empty($data));
            }
            return($empty);
        }

        public static function zipFiles($files) {
            // give the archive a generic name...
            $zip = Site::$config->docroot . self::DIR_EXPORT . self::FIL_EXPORT;

            // ...delete if it already exists...
            if (file_exists($zip)) unlink($zip);

            // ...zip the files into it...
            $z = new \ZipArchive();
            if ($z->open($zip, \ZipArchive::CREATE)) {
                foreach ($files as $f) {
                    $path = explode('/', $f);
                    $name = $path[count($path) - 1];
                    $z->addFile($f, $name);
                }
                $z->close();

                // ...remove the individual recipe files
                foreach ($files as $file) unlink($file);

                return(true);
            }

            return(false);
        }
    }

    Recipes::init();
?>
